/*
===========================================================================
Copyright (C) 2013 - 2015, OpenJK contributors

This file is part of the OpenJK source code.

OpenJK is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see <http://www.gnu.org/licenses/>.
===========================================================================
*/

// sv_gametypeapi.cpp  -- interface to the gametype dll
//Anything above this #include will be ignored by the compiler

#include "server.h"
#include "botlib/botlib.h"
#include "ghoul2/ghoul2_shared.h"
#include "qcommon/cm_public.h"
#include "qcommon/timing.h"

#include "qcommon/GenericParser2.h"
#include "sv_gametypeapi.h"

// gametype interface
static vm_t *gtvm; // gametype vm, valid for legacy and new api
//
// gametype vmMain calls
//
void GTVM_InitGametype()
{
	// On init request create GT VM
	SV_BindGametype();
	Com_Printf("----------------- Gametype Initialization -----------------\n");
	VM_Call(gtvm, GAMETYPE_INIT);
}

void GTVM_Event(intptr_t arg0, intptr_t arg1, intptr_t arg2, intptr_t arg3, intptr_t arg4, intptr_t arg5, intptr_t arg6)
{
	VM_Call(gtvm, GAMETYPE_EVENT, arg0, arg1, arg2, arg3, arg4, arg5, arg6);
}

void GTVM_RunFrame(intptr_t arg0)
{
	VM_Call(gtvm, GAMETYPE_RUN_FRAME, arg0);
}

void GTVM_CvarRegister(vmCvar_t *vmCvar, const char *varName, const char *defaultValue, int flags) {
	Com_Printf("GTVM_CvarRegister(): Registering cvar %s, default value: %s\n", varName, defaultValue);
}

intptr_t SV_GameTypeSystemCalls(intptr_t *args) {
	switch (args[0]) {
	case GT_CVAR_REGISTER:			// ( vmCvar_t *vmCvar, const char *varName, const char *defaultValue, int flags );
		GTVM_CvarRegister((vmCvar_t *)VMA(1), (const char *)VMA(2), (const char *)VMA(3), (int)VMA(4));
		break;
	case GT_CVAR_UPDATE:
		Cvar_Update((vmCvar_t *)VMA(1));
		break;
	case GT_MEMSET:
		Com_Memset(VMA(1), args[2], args[3]);
		break;
	default:
		//Com_Error(ERR_DROP, "Bad gametype system trap: %ld", (long int)args[0]);
		Com_Printf("WARNING: Gametype system trap not implemented: %ld\n", (long int)args[0]);
	}
	return -1;
}

void SV_BindGametype() {
	gtvm = VM_Create(VM_GAMETYPE, SV_GameTypeSystemCalls, (vmInterpret_t)Cvar_VariableIntegerValue("vm_gametype"));
	if (!gtvm) {
		Com_Error(ERR_DROP, "VM_Create on gametype failed");
	}
}

void SV_UnbindGametype() {
	VM_Free(gtvm);
	gtvm = NULL;
}